const staticCacheName = 'site-static-v4'
const dynamicCacheName = 'site-dynamic-v4'
const assets = [
	'/logo192.png',
	'/logo512.png',
	'/mask512.png',
	'/logoApple.png'
]

// install event
self.addEventListener('install', evt => {
	//console.log('service worker installed');
	evt.waitUntil(
		caches.open(staticCacheName).then((cache) => {
			cache.addAll(assets)
		})
	)
})

// activate event
self.addEventListener('activate', evt => {
	//console.log('service worker activated');
	evt.waitUntil(
		caches.keys().then(keys => {
			//console.log(keys);
			return Promise.all(keys
				.filter(key => key !== staticCacheName && key !== dynamicCacheName)
				.map(key => caches.delete(key))
			)
		})
	)
})

// Fetch Events => Caching Fetch Events' Responses
self.addEventListener("fetch", (evt) => {
	let doSave = false;
	const includeCache = [".svg", ".png", ".jpg", ".webp", ".gif", ".mp4", ".mov", ".mp3", ".css"];
	includeCache.find((thisWord) => {
		doSave = evt?.request?.url?.includes?.(thisWord);
		return doSave;
	});

	if (!doSave) {
		return evt;
	}

	evt.respondWith(
		caches.match(evt.request).then((cacheRes) => {
			function fetchBrandNew() {
				return fetch(evt.request).then((fetchRes) => {
					return caches.open(dynamicCacheName).then((cache) => {
						cache.put(evt.request.url, fetchRes.clone());
						// check cached items size
						return fetchRes;
					});
				});
			}

			return cacheRes || fetchBrandNew();
		}),
	);
});
