import {
	BrowserRouter
} from 'react-router-dom'

import {
	QueryClient,
	QueryClientProvider,
} from 'react-query'

import { LanguageProvider } from './contexts/Language'
import MainLayout from './layouts/Main'
import Router from './Router'

const queryClient = new QueryClient()

export default function App() {
	return (
		<QueryClientProvider client={queryClient}>
			<BrowserRouter>
				<LanguageProvider>
					<MainLayout>
						<Router />
					</MainLayout>
				</LanguageProvider>
			</BrowserRouter>
		</QueryClientProvider>
	)
}
