import { useState, useEffect, createContext, useContext } from 'react';
import { useNavigate } from 'react-router';

interface LanguageContextType {
    language: string;
    changeLang: (newLang: string) => void;
}

const LanguageContext = createContext<LanguageContextType>({
    language: 'en',
    changeLang: () => { }
});

type Props = {
    children: string | JSX.Element | JSX.Element[]
}

const CHOSEN_LANG = 'chosen-lang-qmes'
// const availableLanguages = ['en', 'pt'];

export function LanguageProvider({ children }: Props) {
    const defaultLang = 'en'
    const [language, setLanguage] = useState(defaultLang)
    const navigator = useNavigate()

    function initialLanguageFromBrowser() {
        try {
            const langFromStorage = localStorage.getItem(CHOSEN_LANG);
            if (langFromStorage) {
                setLanguage(langFromStorage);
                return;
            }

            // const browserLanguage = window?.navigator?.language || defaultLang;
            // const cleanLanguage = browserLanguage?.split('-')?.[0] || defaultLang;
            // if (!availableLanguages.includes(cleanLanguage)) return;
            // setLanguage(cleanLanguage);
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        initialLanguageFromBrowser()
    }, [])

    function changeLang(newLang: string) {
        setLanguage(newLang)
        localStorage.setItem(CHOSEN_LANG, newLang);
        navigator('/')
    }

    const value = {
        language,
        changeLang,
    }

    return (
        <LanguageContext.Provider value={value}>
            {children}
        </LanguageContext.Provider>
    )
}


export function useLanguage() {
    return useContext(LanguageContext);
}
