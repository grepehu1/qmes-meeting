import './styles.scss'
import BRFlag from '../../assets/imgs/BRFlagIcon.svg'
import USFlag from '../../assets/imgs/USFlagIcon.svg'
import { useLanguage } from '../../contexts/Language'

export default function LangSwitcher({ isHeader }: { isHeader?: boolean }) {
    const { language, changeLang } = useLanguage()

    function currentFlag() {
        switch (language) {
            case 'pt':
                return BRFlag
                break;
            case 'en':
            default:
                return USFlag
                break;
        }
    }

    return (
        <div className={`lang-switch ${isHeader ? 'lang-switch-in-header' : ''}`}>
            <button className='main-lang-switch-btn' title="Language Switch" aria-label="Language Switch">
                <img className="lang-current-icon" src={currentFlag()} />
            </button>
            <ul className="lang-menu">
                <li className="lang-item">
                    <button onClick={() => changeLang('pt')} title="Mudar Linguagem para Português" aria-label="Mudar Linguagem para Português">
                        <img src={BRFlag} />
                    </button>
                </li>
                <li className="lang-item">
                    <button onClick={() => changeLang('en')} title="Change Language to English" aria-label="Change Language to English">
                        <img src={USFlag} />
                    </button>
                </li>
            </ul>
        </div>
    )
}
