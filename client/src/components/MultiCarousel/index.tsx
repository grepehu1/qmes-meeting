import './styles.scss'
import { useState } from 'react';

interface MultiCarouselProps {
    imagesURLs: string[];
    galleryLink?: string;
}

export default function MultiCarousel({ imagesURLs, galleryLink }: MultiCarouselProps) {
    const [currentImageIndex, setCurrentImageIndex] = useState(0);

    const handlePrevClick = () => {
        setCurrentImageIndex((prevIndex) => (prevIndex === 0 ? imagesURLs.length - 1 : prevIndex - 1));
    };

    const handleNextClick = () => {
        setCurrentImageIndex((prevIndex) => (prevIndex === imagesURLs.length - 1 ? 0 : prevIndex + 1));
    };

    return (
        <>
            <div className="image-multi-carousel">
                <button className="arrow prev" onClick={handlePrevClick}>&lt;</button>
                <img className="current-image" src={imagesURLs[currentImageIndex]} alt={`Image ${currentImageIndex + 1}`} />
                <button className="arrow next" onClick={handleNextClick}>&gt;</button>
            </div>
            {galleryLink ? (
                <div className='image-multi-carousel-gallery'>
                    <a href={galleryLink} target='_blank' rel="noopener noreferrer">Check the Full Gallery</a>
                </div>
            ) : <></>}
        </>
    )
}