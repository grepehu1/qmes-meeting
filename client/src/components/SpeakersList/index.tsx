import { useQuery } from 'react-query'
import { Speaker, getSpeakers } from '../../firebase/functions'
import SpeakersListItem from './SpeakersListItem'
import './styles.scss'
import { useLanguage } from '../../contexts/Language'

interface SpeakersListProps {
	title?: string
}

export default function SpeakersList({ title }: SpeakersListProps) {
	const { language } = useLanguage()
	const { data: speakers, isLoading, error } = useQuery<Speaker[], Error>('speakers-' + language, () => getSpeakers(language))

	function renderSpeakers() {
		if (isLoading) return <></>
		if (error) return <></>
		if (!speakers || !speakers.length) return <></>

		const chunkSize = 2

		const speakersInChunks = speakers.reduce((resultArray: Speaker[][], item, index) => {
			const chunkIndex = Math.floor(index / chunkSize)

			if (!resultArray[chunkIndex]) {
				resultArray[chunkIndex] = [] // start a new chunk
			}

			resultArray[chunkIndex].push(item)

			return resultArray
		}, [])

		return speakersInChunks?.map(speakersChunk => {
			return (
				<>
					<div className="speakersList-line row">
						{renderSpeakerLine(speakersChunk)}
					</div>
				</>
			)
		})

	}

	function renderSpeakerLine(speakers: Speaker[]) {
		return speakers?.map((speaker, index) => {
			return (
				<SpeakersListItem key={`speakersList-card-${index}`} speaker={speaker} />
			)
		})
	}

	return (
		<div className="speakersList-wrapper container">
			{title ? <h2 className="speakersList-title">{title}</h2> : <></>}
			<div className="speakersList-cards">
				{renderSpeakers()}
			</div>
		</div>
	)
}
