export default {
    test: 'teste',
    websiteBy: 'Estrutura de Website desenvolvida por',
    emailAlreadyRegistered: 'Este email já foi registrado para o evento!',
    thankYouRegistration: 'Obrigado pela sua inscrição!',
    youllBeReceivingEmailWithPaymentLink: 'Você irá receber um email com o link de pagamento da inscrição em breve, lembre-se de enviar de volta o comprovante de pagamento para que possamos validar a sua inscrição!',
}