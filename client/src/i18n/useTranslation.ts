import { useLanguage } from '../contexts/Language';
import { useCallback } from 'react';

import en from './en';
import pt from './pt';

const labels: { [key: string]: { [key: string]: string } } = {
    en,
    pt,
};

export function translateByLocale(
    locale: string,
    defaultLocale: string,
    key: string
): string {
    return (
        labels?.[locale]?.[key] ||
        labels?.[defaultLocale]?.[key] ||
        'MISSING TRANSLATION'
    );
}

export function useTranslation() {
    const { language } = useLanguage()

    const t = useCallback(
        (key: string): string => {
            return translateByLocale(language, 'en', key);
        },
        [language]
    );

    return { t };
}