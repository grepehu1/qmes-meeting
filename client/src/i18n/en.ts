export default {
    test: 'test',
    websiteBy: 'Website structure developed by',
    emailAlreadyRegistered: 'This email has already been registered for the event!',
    thankYouRegistration: 'Thank you for your registration!',
    youllBeReceivingEmailWithPaymentLink: 'You\'ll be receiving an email with the registration payment link soon, remember to forward back the payment proof so we can validate your registration!',
}