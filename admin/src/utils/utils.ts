import emailjs from '@emailjs/browser'

const linksPayment = {
    full: 'https://buy.stripe.com/28o4hA1TTdcS4YU3cc',
    noSupport: 'https://buy.stripe.com/28o15obut7Sy3UQeUV'
}

export function sendRegistrationConfirmationEmail(
    email: string,
    value: string,
    link: 'full' | 'noSupport',
) {
    const templateParams = {
        to_mail: email,
        value: value,
        link: linksPayment[link],
    };

    // Don't waste SMTP usage
    const env = import.meta.env.VITE_ENV
    if (env && env === 'dev') return

    const serviceId = import.meta.env.VITE_EJS_SERVICE_ID
    const templateId = import.meta.env.VITE_EJS_TEMPLATE_ID
    const userId = import.meta.env.VITE_EJS_USER_ID

    if (!serviceId || !templateId || !userId) return

    emailjs.send(
        serviceId,
        templateId,
        templateParams,
        userId
    )
        .then(function (response) {
            console.log('SUCCESS!', response.status, response.text);
        })
        .catch(e => console.log(e))
}