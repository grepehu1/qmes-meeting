import { buildProperty } from "firecms";

export const multiCarousel = buildProperty({
    dataType: "map",
    properties: {
        content: {
            name: "Image Links",
            description: "Links for The Images to be shown (https://postimages.org/)",
            dataType: "string",
            multiline: true,
        },
        galleryLink: {
            name: "Galery Link",
            description: "Links to The Images gallery (https://postimages.org/)",
            dataType: "string",
            url: true
        },
    }
});