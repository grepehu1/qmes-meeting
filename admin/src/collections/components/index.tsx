import { articleList } from "./article-list";
import { eventRegister } from "./event-register";
import { speakersList } from "./speaker-list"
import { contactForm } from "./contact-form";
import { carousel } from "./carousel";
import { multiCarousel } from "./multi-carousel";

export {
    articleList,
    eventRegister,
    speakersList,
    contactForm,
    carousel,
    multiCarousel,
}