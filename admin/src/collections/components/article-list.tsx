import { buildProperty } from "firecms";

export const articleList = buildProperty({
    dataType: "map",
    properties: {
        title: {
            name: "Title",
            description: "Text shown above list",
            validation: { required: true },
            dataType: "string"
        }
    }
});