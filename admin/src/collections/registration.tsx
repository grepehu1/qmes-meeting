import {
	buildCollection,
	buildProperty,
	EntityReference,
	buildEntityCallbacks,
	EntityOnSaveProps,
} from 'firecms';
import { sendRegistrationConfirmationEmail } from '../utils/utils';

type Registration = {
	name: string;
	createdDate: Date;
	accommodation: boolean;
	position: string;
	purpose: string;
	institute: string
	email: string;
	hasPoster: boolean;
	subject: string;
	posterFile: EntityReference;
	paymentType: string;
	noSupportPaymentFile: EntityReference;
	noSupportPaymentFileVerified: boolean;
	paymentVerified: boolean;
}

const registrationCollection = buildCollection<Registration>({
	name: 'Registrations',
	singularName: 'Registration',
	path: 'registrations',
	inlineEditing: false,
	group: 'Main',
	defaultSize: "s",
	icon: 'EventSeat',
	initialSort: ["createdDate", "desc"],
	permissions: ({ authController }) => ({
		edit: true,
		create: true,
		delete: true
	}),
	callbacks: buildEntityCallbacks({
		onSaveSuccess: (props: EntityOnSaveProps<Registration>) => {
			const previousNoSupportPaymentFileVerified = props?.previousValues?.noSupportPaymentFileVerified
			const newNoSupportPaymentFileVerified = props?.values?.noSupportPaymentFileVerified

			const hasItChangedToTrue = !previousNoSupportPaymentFileVerified && newNoSupportPaymentFileVerified
			// Email here
			if (!hasItChangedToTrue) return
			sendRegistrationConfirmationEmail(
				props.values.email || '',
				props.values.paymentType === 'full' ? '150.00' : '50.00',
				props.values.paymentType === 'full' ? 'full' : 'noSupport',
			)
		}
	}),
	properties: {
		name: {
			name: 'Name',
			validation: { required: true },
			dataType: 'string'
		},
		createdDate: buildProperty({
			dataType: "date",
			name: "Registration Time",
			mode: "date_time"
		}),
		accommodation: buildProperty({
			name: 'With Accommodation?',
			description: 'Has this person opted in for accommodation?',
			dataType: 'boolean'
		}),
		position: buildProperty({
			name: 'Position',
			description: 'Student/Professor/other',
			dataType: 'string',
			enumValues: {
				student: "Student",
				professor: "Professor",
				other: "Other"
			}
		}),
		purpose: buildProperty({
			name: 'Purpose',
			description: 'If the registration is regarding a Listener, Speaker or Poster submitter.',
			dataType: 'string',
			enumValues: {
				listener: "Participant",
				speaker: "Speaker",
				poster: "Poster"
			}
		}),
		institute: {
			name: 'Affiliated Institute',
			dataType: 'string',
		},
		email: {
			name: 'E-mail',
			validation: { required: true },
			dataType: 'string',
		},
		hasPoster: buildProperty({
			name: 'Put Up Poster?',
			description: 'Has this person opted in for putting up a work\'s poster?',
			dataType: 'boolean'
		}),
		subject: {
			name: 'Poster\'s Subject',
			dataType: 'string',
		},
		posterFile: buildProperty({
			dataType: "reference",
			path: "files",
			name: "Poster\'s File",
		}),
		paymentType: {
			name: 'Payment Type',
			dataType: 'string',
			enumValues: {
				full: "Full Payment",
				noSupport: "Payment With no Financial Support",
			}
		},
		noSupportPaymentFile: buildProperty({
			dataType: "reference",
			path: "files",
			name: "No Financial Support Proof file",
		}),
		noSupportPaymentFileVerified: buildProperty({
			name: 'No Financial Support Verified?',
			description: 'Has this person no financial support file been verified?',
			dataType: 'boolean'
		}),
		paymentVerified: buildProperty({
			name: 'Payment Verified',
			description: 'Has this person registration been verified?',
			dataType: 'boolean'
		}),
	},
});

export default registrationCollection